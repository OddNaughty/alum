/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alum.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 20:23:06 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 23:28:34 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef P4_H
# define P4_H

# include <errno.h>
# include <string.h>
# define SUCCESS 0
# define FAILURE -1
# define BUF_SIZE 16

# define EMPTY '.'
# define PLAYER 'X'
# define IA 'O'

typedef struct		s_coo
{
	int				x;
	int				y;
}					t_coo;

typedef struct		s_p4
{
	char	**board;
	t_coo	board_size;
	t_coo	last_play;
	int		win;
	int		turn;
	int		verify;
	int		token_max;
}					t_p4;


/*
**File: error.c
*/
int		alum_error(char *str);
void	*alum_errmalloc(char *str);

/*
**File: init.c
*/
char	*init_raw(int w);
char	**init_board(int h, int w);

/*
**File: print.c
*/
void	printboard(t_p4 *game);

/*
**File: game.c
*/
int		put_in_col(t_p4 *game, int col);
int		launch_game(t_p4 *game);

/*
**File: ia.c
*/
int		where_put(t_p4 *game);
int		check_all(t_p4 *game, t_coo pos, int len);
int		check_player_win(t_p4 *env, int len);
int		you_cant_win(t_p4 *game);

/*
**File: check.c 
*/
int		check_s(t_p4 *game, t_coo pos, int len);
int		check_e(t_p4 *game, t_coo pos, int len);
int		check_w(t_p4 *game, t_coo pos, int len);
int		check_sw(t_p4 *game, t_coo pos, int len);
int		check_se(t_p4 *game, t_coo pos, int len);
int		check_nw(t_p4 *game, t_coo pos, int len);
int		check_ne(t_p4 *game, t_coo pos, int len);

/*
**Directory: libft
*/
void	ft_strdel(char **as);
int		ft_freechartab(char ***table);
int		ft_atoi(const char *str);
void	ft_putchar(char c);
void	ft_putstr(char const *s);
void	ft_putendl(char const *s);
void	ft_putnbr(int n);
void	ft_putstr_fd(char const *s, int fd);
void	ft_putendl_fd(char const *s, int fd);
size_t	ft_strlen(const char *str);
int		get_next_line(int const fd, char** line);
char	*ft_strdup(const char *s1);
void	*ft_memcpy(void * s1, const void * s2, size_t n);
char	*ft_strnew(size_t size);

#endif
