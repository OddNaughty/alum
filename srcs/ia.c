/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fcorbel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 16:30:35 by fcorbel           #+#    #+#             */
/*   Updated: 2014/03/09 23:31:36 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"
#include <stdlib.h>
#include <time.h>

int			check_all(t_p4 *game, t_coo pos, int len)
{
	int		i;
	int		ret;
	int		col;
	int		(*check[7])(t_p4 *game, t_coo pos, int len) =
		{check_s,
		check_e,
		check_w,
		check_sw,
		check_se,
		check_nw,
		check_ne
		};

	i = 0;
	col = FAILURE;
	while (i < 7)
	{
		if ((ret = check[i](game, pos, len)) != FAILURE)
			col = ret;
		i++;
	}
	return (col);
}

int			check_player_win(t_p4 *game, int len)
{
	t_coo	pos;
	int		col;
	int		ret;

	pos.y = -1;
	ret = FAILURE;
	while (++pos.y < game->board_size.y)
	{
		pos.x = -1;
		while (++pos.x < game->board_size.x)
		{
			if (game->board[pos.y][pos.x] == game->verify)
			{
				if ((col = check_all(game, pos, len)) != FAILURE)
					ret = col;
			}
		}
	}
	return (ret);
}

int		where_put(t_p4 *game)
{
	int		col;
	int		ia;

	game->verify = IA;
	if ((ia = check_player_win(game, 4)) != FAILURE)
		game->win = IA;
	game->verify = PLAYER;
	if ((col = check_player_win(game, 4)) == FAILURE)
	{
		while ((col < 1) || (col > game->board_size.x)
			   || (game->board[0][col - 1] != EMPTY))
			col = rand() % (game->board_size.x + 1);
	}
	if ((game->win != PLAYER) && (ia != FAILURE))
		return (ia);
	return (col);
}

int			you_cant_win(t_p4 *game)
{
	int		col;

	if (game->last_play.x != -1)
		col = where_put(game);
	else
		col = game->board_size.x / 2 + 1;
	return (col);
}
