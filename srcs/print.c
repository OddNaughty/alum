/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 17:04:39 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 21:53:20 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

static void	printnorme(char **board, int i)
{
	int		j;

	j = 0;
	while (board[i][j])
	{
		ft_putstr((board[i][j] == PLAYER)? "\033[31m": "");
		ft_putstr((board[i][j] == IA)? "\033[34m": "");
		ft_putchar(board[i][j]);
		ft_putstr("\033[0m");
		ft_putstr(" ");
		j++;
	}
}

void		printboard(t_p4 *game)
{
	int		i;
	int		cmp;

	i = cmp = 0;
	while (game->board[i])
	{
		ft_putstr("| ");
		printnorme(game->board, i);
		ft_putendl("|");
		i++;
	}
	ft_putstr("  ");
	while (cmp < game->board_size.x)
	{
		ft_putnbr(cmp + 1);
		ft_putstr(" ");
		cmp++;
	}
	ft_putchar('\n');
}
