/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 16:36:40 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 20:43:54 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"
#include <stdlib.h>

char		*init_raw(int w)
{
	char	*new;
	int		i;

	if ((new = ft_strnew(w)) == NULL)
		return ((char *)alum_errmalloc("In init_raw: "));
	i = 0;
	while (i < w)
	{
		new[i] = EMPTY;
		i++;
	}
	return (new);
}

char		**init_board(int h, int w)
{
	char	**new;
	int		i;

	if ((new = malloc(sizeof(*new) * (h + 1))) == NULL)
		return ((char **)alum_errmalloc("In alum_alloc: "));
	i = 0;
	while (i < h)
	{
		if ((new[i] = init_raw(w)) == NULL)
			return (NULL);
		i++;
	}
	new[i] = NULL;
	return (new);
}
