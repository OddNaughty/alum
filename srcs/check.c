/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 12:17:04 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 23:12:32 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

int			check_s(t_p4 *game, t_coo pos, int len)
{
	int		i;
	char	**map;
	int		player;

	i = player = 0;
	map = game->board;
	while (i < len)
	{
		if ((pos.y + i < game->board_size.y)
			&& (map[pos.y + i][pos.x] == game->verify))
			player++;
		i++;
	}
	if ((player == 3) && (pos.y - 1 >= 0)
		&& (map[pos.y - 1][pos.x] == EMPTY))
		return (pos.x + 1);
	if (player == 4)
		game->win = game->verify;
	return (FAILURE);
}


int			check_e(t_p4 *game, t_coo pos, int len)
{
	int		i;
	char	**map;
	int		player;
	int		empty;

	i = player = 0;
	empty = -1;
	map = game->board;
	while (i < len)
	{
		if ((pos.x + i < game->board_size.x)
			&& (map[pos.y][pos.x + i] == game->verify))
			player++;
		if ((pos.x + i < game->board_size.x)
			&& (map[pos.y][pos.x + i] == EMPTY))
			empty = pos.x + i;
		i++;
	}
	if ((player == 3) && (empty != -1) && (((pos.y + 1 < game->board_size.y)
		&& (map[pos.y + 1][empty] != EMPTY))
		|| (pos.y == game->board_size.y - 1)))
		return (empty + 1);
	if (player == 4)
		game->win = game->verify;
	return (FAILURE);
}

int			check_w(t_p4 *game, t_coo pos, int len)
{
	int		i;
	char	**map;
	int		player;
	int		empty;

	i = player = 0;
	empty = -1;
	map = game->board;
	while (i < len)
	{
		if ((pos.x - i >= 0) && (map[pos.y][pos.x - i] == game->verify))
			player++;
		if ((pos.x - i >= 0) && (map[pos.y][pos.x - i] == EMPTY))
			empty = pos.x - i;
		i++;
	}
	if ((player == 3) && (empty != -1) && (((pos.y + 1 < game->board_size.y)
		&& (map[pos.y + 1][empty] != EMPTY))
		|| (pos.y == game->board_size.y - 1)))
		return (empty + 1);
	if (player == 4)
		game->win = game->verify;
	return (FAILURE);
}
