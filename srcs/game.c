/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 17:36:10 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 23:22:36 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"
#include <time.h>
#include <stdlib.h>

int			put_in_col(t_p4 *game, int col)
{
	char	**tab;
	int		y;

	tab = game->board;
	y = 1;
	game->last_play.x = col - 1;
	while (y < game->board_size.y)
	{
		if (tab[y][col - 1] != EMPTY)
		{
			game->board[y - 1][col - 1] = game->turn;
			game->last_play.y = y - 1;
			return (SUCCESS);
		}
		y++;
	}
	game->board[y - 1][col - 1] = game->turn;
	game->last_play.y = y - 1;
	return (SUCCESS);
}

static int	human_turn(t_p4 *game)
{
	char	*str;
	int		col;

	str = NULL;
	col = 0;
	while ((col < 1) || (col > game->board_size.x)
			|| (game->board[0][col - 1] != EMPTY))
	{
		ft_putendl("Please enter a true position mofo");
		if (get_next_line(0, &str) == FAILURE)
			return ((int)alum_error("GNL FAILURE"));
		col = ft_atoi(str);
		ft_strdel(&str);
	}
	if (put_in_col(game, col) == FAILURE)
		return (human_turn(game));
	return (SUCCESS);
}

static void	display(t_p4 *game)
{
	ft_putstr("\033[H\033[2J");
	ft_putstr("\033[0m");
	printboard(game);
}

static int	turning(t_p4 *game, int turn)
{
	if (!turn)
	{
		game->turn = IA;
		put_in_col(game, you_cant_win(game));
		turn = 1;
	}
	else
	{
		game->turn = PLAYER;
		human_turn(game);
		turn = 0;
	}
	return (turn);
}

int			launch_game(t_p4 *game)
{
	int		turn;
	int		token;

	srand(time(NULL));
	token = 0;
	turn = rand() % 2;
	while (!game->win)
	{
		display(game);
		turn = turning(game, turn);
		if (++token == game->token_max)
		{
			ft_putendl("END OF GAME");
			break ;
		}
	}
	if (game->win == IA)
		display(game);
	ft_putendl((game->win == PLAYER)? 
				"\033[32mYou win. GG, you p0wned a hand-crafted AI\033[0m": 
				"\033[31mYou lose. OMG so n00b.\033[0m");
	return (SUCCESS);
}
