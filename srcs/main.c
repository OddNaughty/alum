/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 20:24:15 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 21:17:29 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

int		main(int ac, char **av)
{
	t_p4	game;

	if (ac != 3)
		return (alum_error("I need height & width"));
	else if ((ft_atoi(av[1]) < 6) || (ft_atoi(av[2]) < 7))
		return (alum_error("Minimal width = 7 & minimal height = 6"));
	game.board_size.y = ft_atoi(av[1]);
	game.board_size.x = ft_atoi(av[2]);
	game.last_play.x = -1;
	game.last_play.y = -1;
	game.token_max = game.board_size.y * game.board_size.x;
	if ((game.board = init_board(game.board_size.y, game.board_size.x)) == NULL)
		return (FAILURE);
	if (launch_game(&game) == FAILURE)
		return (FAILURE);
	ft_freechartab(&(game.board));
	return (SUCCESS);
}
