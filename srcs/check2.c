/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 20:56:49 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 21:24:28 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

int			check_sw(t_p4 *game, t_coo pos, int len)
{
	int		i;
	int		player;
	t_coo	empty;

	i = player = 0;
	empty.y = -1;
	while (i < len)
	{
		if ((pos.x - i >= 0) && (pos.y + i < game->board_size.y)
			&& (game->board[pos.y + i][pos.x - i] == game->verify))
			player++;
		if ((pos.x - i >= 0) && (pos.y + i < game->board_size.y)
			&& (game->board[pos.y + i][pos.x - i] == EMPTY))
		{
			empty.x = pos.x - i;
			empty.y = pos.y + i;
		}
		i++;
	}
	if ((player == 3) && (empty.y != -1) && (empty.y + 1 < game->board_size.y)
		&& (game->board[empty.y + 1][empty.x] != EMPTY))
		return (empty.x + 1);
	if (player == 4)
		game->win = game->verify;
	return (FAILURE);
}

int			check_nw(t_p4 *game, t_coo pos, int len)
{
	int		i;
	int		player;
	t_coo	empty;

	i = player = 0;
	empty.y = -1;
	while (i < len)
	{
		if ((pos.x - i >= 0) && (pos.y - i >= 0)
			&& (game->board[pos.y - i][pos.x - i] == game->verify))
			player++;
		if ((pos.x - i >= 0) && (pos.y - i >= 0)
			&& (game->board[pos.y - i][pos.x - i] == EMPTY))
		{
			empty.x = pos.x - i;
			empty.y = pos.y - i;
		}
		i++;
	}
	if ((player == 3) && (empty.y != -1) && (empty.y + 1 < game->board_size.y)
		&& (game->board[empty.y + 1][empty.x] != EMPTY))
		return (empty.x + 1);
	if (player == 4)
		game->win = game->verify;
	return (FAILURE);
}

int			check_ne(t_p4 *game, t_coo pos, int len)
{
	int		i;
	int		player;
	t_coo	empty;

	i = player = 0;
	empty.y = -1;
	while (i < len)
	{
		if ((pos.x + i < game->board_size.x) && (pos.y - i >= 0)
			&& (game->board[pos.y - i][pos.x + i] == game->verify))
			player++;
		if ((pos.x + i < game->board_size.x) && (pos.y - i >= 0)
			&& (game->board[pos.y - i][pos.x + i] == EMPTY))
		{
			empty.x = pos.x + i;
			empty.y = pos.y - i;
		}
		i++;
	}
	if ((player == 3) && (empty.y != -1) && (empty.y + 1 < game->board_size.y)
		&& (game->board[empty.y + 1][empty.x] != EMPTY))
		return (empty.x + 1);
	if (player == 4)
		game->win = game->verify;
	return (FAILURE);
}

int			check_se(t_p4 *game, t_coo pos, int len)
{
	int		i;
	int		player;
	t_coo	empty;

	i = player = 0;
	empty.y = -1;
	while (i < len)
	{
		if ((pos.x + i < game->board_size.x) && (pos.y + i < game->board_size.y)
			&& (game->board[pos.y + i][pos.x + i] == game->verify))
			player++;
		if ((pos.x + i < game->board_size.x) && (pos.y + i < game->board_size.y)
			&& (game->board[pos.y + i][pos.x + i] == EMPTY))
		{
			empty.x = pos.x + i;
			empty.y = pos.y + i;
		}
		i++;
	}
	if ((player == 3) && (empty.y != -1) && (empty.y + 1 < game->board_size.y)
		&& (game->board[empty.y + 1][empty.x] != EMPTY))
		return (empty.x + 1);
	if (player == 4)
		game->win = game->verify;
	return (FAILURE);
}
