/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 20:28:11 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 20:39:59 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

/*
**Display error message str and return FAILURE
*/

int		alum_error(char *str)
{
	ft_putstr_fd("Error: ", 2);
	ft_putendl_fd(str, 2);
	return (FAILURE);
}

/*
**Display error message and what errno says and return FAILURE
*/
void	*alum_errmalloc(char *str)
{
	ft_putstr_fd(str, 2);
	ft_putendl_fd(strerror(errno), 2);
	return (NULL);
}
