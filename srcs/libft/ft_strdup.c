/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 20:49:53 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/09 20:49:56 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"
#include <stdlib.h>

char	*ft_strdup(const char *s1)
{
	char			*_s1;
	unsigned int	len;

	len = ft_strlen(s1);
	_s1 = (char *) malloc ((1 + len) * sizeof(char));
	if (_s1 == NULL)
		return (NULL);
	ft_memcpy(_s1, s1, len + 1);
	return (_s1);
}
