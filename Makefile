#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/25 12:34:27 by cwagner           #+#    #+#              #
#    Updated: 2014/03/09 20:38:17 by cwagner          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = 		puissance4

CC = 		gcc
CFLAGS = 	-Wall -Wextra -Werror $(DEBUG)
INCLUDES = 	-I includes/

SRCSDIR =	srcs/
SRCSFILES =	main.c\
			error.c\
			init.c\
			print.c\
			game.c\
			ia.c\
			check.c\
			check2.c
SRCS =		$(addprefix $(SRCSDIR), $(SRCSFILES))

LIBDIR = 	$(SRCSDIR)libft/
LIBFILES =	ft_strdel.c\
			ft_freechartab.c\
			ft_atoi.c\
			ft_putchar.c\
			ft_putstr.c\
			ft_putendl.c\
			ft_putnbr.c\
			ft_putstr_fd.c\
			ft_putendl_fd.c\
			ft_strlen.c\
			get_next_line.c\
			ft_strdup.c\
			ft_strnew.c\
			ft_memcpy.c

LIB =		$(addprefix $(LIBDIR), $(LIBFILES))

ALL =		$(SRCS) $(LIB)

OBJ = 		$(ALL:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)	
	@echo "\033[32m[Building] \033[0m" | tr -d '\n'
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $(OBJ)

%.o: %.c
	@echo "\033[33m[Doing object] \033[0m" | tr -d '\n'
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<

clean:
	@echo "\033[34m[Cleaning] \033[0m" | tr -d '\n'
	rm -f $(OBJ)

fclean: clean
	@echo "\033[34m[Filecleaning] \033[0m" | tr -d '\n'
	rm -f $(NAME)

re: fclean all

run: all
	@echo "\033[32m[Running Binary]\033[0m" | tr -d '\n'
	@echo ""
	@./$(NAME) 6 7
	@make clean

debug: re
	lldb ./$(NAME)

.PHONY: clean fclean re
